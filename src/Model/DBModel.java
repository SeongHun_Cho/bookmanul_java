package Model;

import java.io.IOException;
import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class DBModel {

	// DB연동부분을 하나의 클래스에 다 넣고 그걸 객체화해서 사용하자
	Connection connection = null;
	Statement stmt = null;
	ResultSet excute = null;
	List<Model.BookClass> bookList = new ArrayList<Model.BookClass>();
	List<Model.MemberClass> memberList = new ArrayList<Model.MemberClass>();
	List<String> borrowList = new ArrayList<String>();
	Scanner scanner = new Scanner(System.in);
	List<String> reportList = new ArrayList<String>();

	// DB 연결 메소드
	public Statement DBConnect() {

		// DB에 연동하기 위한 서버주소와 userId, userPW
		String url = "jdbc:mariadb://127.0.0.1:3307/library";
		String user = "root";
		String password = "0808";

		// 드라이버 로드함
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// System.out.println("드라이버로드실패");
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection(url, user, password);
			if (connection != null) {
				stmt = connection.createStatement();
				// System.out.println("DB접속 성공");
			}
		} catch (SQLException e) {
			// System.out.println("DB접속 실패");
			e.printStackTrace();
		}
		return stmt;
	}

	// 책 검색 메소드 매개변수로 책이름을 넣으면 책이름으로 DB에 연결해서 검색,책이아니라 회원검색 및 회원리스트 출력도 가능, 리스트에 각
	// 값을 객체로 담은 뒤에 리스트를 반환
	// table은 검색할 테이블을 말함(회원이나 북이 될 것), where은 조건의 내용을 말함(bookid면 도서검색, memberid면
	// 회원검색)
	public List<BookClass> DBBookSearch(String allName) {
		stmt = DBConnect();
		String name = allName;
		String query = "";
		bookList.clear();
		// 아무것도 없이 엔터누르면 전체도서리스트 출력
		if (name.equals("")) {
			query = "SELECT * FROM library.book";
		}
		// 그 외의 경우에는 이름이 포함된 책의 리스트를 불러온다
		else {
			query = "SELECT * FROM library.book WHERE bookname Like '%" + name + "%'";
		}
		// 쿼리문을 보낸다
		try {
			excute = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// 객체를 선언해서 리스트에 담아둔다
		try {
			while (excute.next()) {
				Model.BookClass bookInfo = new Model.BookClass();
				bookInfo.setBookId(Integer.parseInt(excute.getString("bookid")));
				bookInfo.setBookName(excute.getString("bookname"));
				bookInfo.setWriter(excute.getString("writer"));
				bookInfo.setCount(Integer.parseInt(excute.getString("count")));
				bookList.add(bookInfo);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return bookList; // 객체로 담겨져있는 리스트 반환
	}

	// 멤버리스트 출력함수
	public List<MemberClass> DBMemberSearch(String Name) {
		stmt = DBConnect();
		String name = Name; // 회원 이름을 매개변수로 받는다
		String query = "";
		memberList.clear();
		// 아무것도 없이 엔터누르면 전체회원리스트 출력
		if (name == "") {
			query = "SELECT * FROM library.member";
		}
		// 그 외의 경우에는 이름이 포함된 회원의 리스트를 불러온다
		else {
			query = "SELECT * FROM library.member WHERE membername LIKE '%" + name + "%'";
		}
		// 쿼리문을 보낸다
		try {
			excute = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// 객체를 선언해서 리스트에 담아둔다
		try {
			while (excute.next()) {
				Model.MemberClass memberInfo = new Model.MemberClass();
				memberInfo.setMemberid(excute.getString("memberid"));
				memberInfo.setPassword(excute.getString("password"));
				memberInfo.setMembername(excute.getString("membername"));
				memberList.add(memberInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList; // 객체로 담겨져있는 리스트 반환
	}

	// 유저나 도서Id를 넣으면 Name값을 출력해줌, userbook=1 일 때는 유저, userbook=2일 때는 도서 //반환하는 것은 해당
	// 이름
	public String DBNameSearch(String id, int userbook) {
		int select = userbook;
		stmt = DBConnect();
		ResultSet excute = null;
		String query = "";
		String name = "";
		String resultName = "";
		String searchid = id;
		if (select == 1) {
			query = "SELECT * FROM library.member WHERE memberid= '" + searchid + "'";
			name = "membername";
		} else if (select == 2) {
			query = "SELECT * FROM library.book WHERE bookid= '" + Integer.parseInt(searchid) + "'";
			name = "bookname";
		} else {
			return "fail";
		}

		try {
			excute = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			excute.next();
			resultName = excute.getString(name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultName;
	}

	// SELECT문을 보내는 메소드 작성,resultset값인 execute를 반환한다
	public ResultSet DBSelectQuery(String querystring) {
		stmt = DBConnect();
		ResultSet execute = null;
		String query = querystring;
		try {
			execute = stmt.executeQuery(query); // 값이 없으면 null을 출력
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return execute;
	}

	// a는 테이블, b는 바꿀 값들, c는 조건이 있을 경우 where 조건문 쓸것
	public void DBUpdate(String a, String b, String c) {
		stmt = DBConnect();
		String query = "Update " + a + " SET " + b + " " + c;
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// a는 테이블, b는 그 안에 값들 c는 조건이 있을 경우 where 조건문 쓸것
	public void DBInsert(String a, String b, String c) {
		stmt = DBConnect();
		String query = "INSERT INTO " + a + " VALUES( " + b + " ) " + c;
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// a는 테이블 b는 조건이 있을 경우 where문 쓸 것
	public void DBDelete(String a, String b) {
		stmt = DBConnect();
		String query = "DELETE FROM " + a + " " + b;
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet DBSelect(String a, String b, String c) {
		stmt = DBConnect();
		String query = "SELECT " + a + " FROM " + b + " " + c;
		try {
			excute = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return excute;
	}

	// 매개변수로 쿼리문만 작성해서 넣으면 UPDATE,INSERT,DELETE문을 보내는 메소드 작성
	public void DBUpdateQuery(String querystring) {
		stmt = DBConnect();
		String query = querystring;
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// DB연결을 해제하는 메소드 작성
	/*
	 * public void DBClose(Connection connect) { try{ connect.close(); }
	 * catch(SQLException e) { e.printStackTrace(); } }
	 */

	public List<Model.BookClass> DBBorrowBookList(String identy) {
		String ID = identy;
		DBConnect();
		bookList.clear();
		// renthistory에서 유저가 빌린책들을 찾아서 출력한다,
		String query = "SELECT library.book.* FROM library.book,library.renthistory "
				+ "WHERE book.bookid = renthistory.bookid AND renthistory.memberid='" + ID
				+ "' AND renthistory.state = '대출'";
		excute = DBSelectQuery(query); // select 쿼리문 실행하는 함수를 불러와서 실행한다
		try {
			// DB에서 불러온 값들을 객체에 넣고 리스트에 저장한다
			while (excute.next()) {
				Model.BookClass borrowBook = new Model.BookClass();
				borrowBook.setBookId(Integer.parseInt(excute.getString("bookid")));
				borrowBook.setBookName(excute.getString("bookname"));
				borrowBook.setWriter(excute.getString("writer"));
				borrowBook.setCount(Integer.parseInt(excute.getString("count")));
				bookList.add(borrowBook); // add로 객체를 리스트에 저장
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookList; // 그 리스트를 반환한다
	}

	public List<Model.MemberClass> DBMemberInformation(String identy) {
		String ID = identy; // 아이디를 매개변수로 받음
		stmt = DBConnect(); // DB연결
		String query = "SELECT * FROM library.member WHERE memberid = '" + ID + "'"; // Select 쿼리문을 만듬
		excute = DBSelectQuery(query); // Select문을 실행함
		// 회원 정보를 객체화시켜 리스트에 넣고 반환한다.
		try {
			// 회원리스트에 DB에 있는 회원 정보를 넣는다,while문으로 반복은 했지만 아마 하나만 들어갈것
			while (excute.next()) {
				Model.MemberClass memberInfo = new Model.MemberClass();
				memberInfo.setMemberid(excute.getString("memberid"));
				memberInfo.setMembername(excute.getString("membername"));
				memberInfo.setPassword(excute.getString("password"));
				memberList.add(memberInfo);
			}
			excute.close();
			stmt.close();
			// 출력값이 없으면 else문을 돌린다
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// renthistory에서 대출한 책을 찾기 위한 쿼리문
		query = "SELECT library.book.bookid,library.book.bookname FROM library.book"
				+ ", library.renthistory WHERE renthistory.memberid = '" + ID + "' AND"
				+ "book.bookid = renthistory.bookid AND renthistory.state = '대출'";

		return memberList;
	}

	// 회원정보에 표시할 빌린 책 목록을 출력하는 부분
	public List<String> DBMemberBorrowBook(String identy) {
		String Id = identy;
		DBConnect(); // DB연결
		// renthistory와 book 테이블을 비교해서 renthistory에서의 bookid에 해당하는 bookname을 불러옴
		String query = "SELECT library.book.bookid,library.book.bookname " + "FROM library.book,library.renthistory "
				+ "WHERE renthistory.memberid = '" + Id + "'"
				+ "AND book.bookid=renthistory.bookid AND renthistory.state = '대출'";
		excute = DBSelectQuery(query);
		try {
			while (excute.next()) {
				borrowList.add(excute.getString("bookid") + "  :  " + excute.getString("bookname"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return borrowList;
	}

	// 매니저 로그인하는 메소드
	public boolean DBManagerLogin(String inputPW) {
		stmt = DBConnect();
		// 관리자 회원의 행을 불ㄹ오는 SELECT Query를 짠다
		String query = "SELECT * FROM library.member WHERE memberid='admin'";
		excute = DBSelectQuery(query); // SELECT 쿼리문 실행
		try {
			excute.next(); // 한 줄 이동한다.
			if (excute.getString("password").equals(inputPW)) { // 관리자 비밀번호랑 맞는지 확인한다
				return true; // 맞으면 true반환
			} else {
				return false; // 틀리면 false 반환
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // 그 외의 경우에는 false를 반환한다
	}

	public int DBborrowBookCount(String userId) {
		String ID = userId;
		int amount = 0;
		stmt = DBConnect();
		String query = "SELECT * FROM library.renthistory WHERE memberid ='" + userId + "'" + " AND state='대출'";
		excute = DBSelectQuery(query);

		try {
			excute.last();
			amount = excute.getRow();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return amount;
	}

	// member나 book 삭제하는 쿼리문을 보내는 것
	public void DBDeleteQuery(String no, int select) {
		String query = "";
		stmt = DBConnect();
		if (select == 1) {
			query = "DELETE FROM library.member WHERE memberid='" + no + "'";
		} else if (select == 2) {
			query = "DELETE FROM library.book WHERE bookid='" + Integer.parseInt(no) + "'";
		}

		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<String> DBReport() {
		String query = "";
		String report = "";
		String bookName = "";
		String memberName = "";
		stmt = DBConnect();
		query = "SELECT * FROM library.renthistory";
		excute = DBSelectQuery(query);
		try {
			while (excute.next()) {
				memberName = DBNameSearch(excute.getString("memberid"), 1);
				bookName = DBNameSearch(excute.getString("bookid"), 2);
				if (excute.getString("state").equals("반납")) {
					report = memberName + "(" + excute.getString("memberid") + ")가 " + bookName + "(ID"
							+ excute.getString("bookid") + ")를 대출(" + excute.getString("date") + ") 후 반납("
							+ excute.getString("back") + ")했습니다.";
					reportList.add(report);
				} else if (excute.getString("state").equals("대출")) {
					report = memberName + "(" + excute.getString("memberid") + ")가 " + bookName + "(ID"
							+ excute.getString("bookid") + ")를 대출(" + excute.getString("date") + ")했습니다.";
					reportList.add(report);
				}
			}
			return reportList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	// 회원 ID 중복 방지
	public boolean IdCheck(String identy, int md) {
		String ID = identy;
		stmt = DBConnect();
		if (md == 1) {
			// 회원가입 때 입력한 ID가 중복되는 아이디인지 쿼리문을 보내서 확인함
			excute = DBSelect("*", "library.member", "WHERE memberid='" + ID + "'");
		} else if (md == 2) {
			// 책 등록 때 입력한 ID가 중복되는 아이디인지 쿼리문을 보내서 확인함
			excute = DBSelect("*", "library.book", "WHERE bookid='" + ID + "'");
		}
		try {
			if (excute.first()) {// 중복된 아이디가 있을 경우 false 반환
				return true;
			} else {// 중복된 아이디가 없을 경우 true 반환
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}
}

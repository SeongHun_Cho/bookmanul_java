package Model;

import java.util.List;
import java.util.ArrayList;

public class MemberClass {

	private String memberid;
	private String password;
	private String membername;
	public List<String> borrowBookList = new ArrayList<String>();
	
	
	
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMembername() {
		return membername;
	}
	public void setMembername(String membername) {
		this.membername = membername;
	}
	
	
}

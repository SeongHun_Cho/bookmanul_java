package Controller;


import java.sql.Statement;
import java.util.Scanner;

import Model.DBModel;


import java.util.ArrayList;
import java.util.List;



public class ManagerControl {
	
	Scanner scanner = new Scanner(System.in);
	DBModel dbconnect = new DBModel();
	Statement stmt = null;
	View.AllView view = new View.AllView();
	List<Model.MemberClass> memberList = new ArrayList<Model.MemberClass>();
	
	//회원 검색하는 메소드
	public void MemberSearch() {
		String name = view.MemberSearch();
		memberList = dbconnect.DBMemberSearch(name);
		view.MemberListPrint(memberList);
		
	}

	
}

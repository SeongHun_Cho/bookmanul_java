package Controller;

import java.io.IOException;
import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


public class MemberControl {
	View.AllView view = new View.AllView();
	Connection connection=null;
	Statement stmt=null;
	ResultSet excute = null;
	Model.DBModel dbconnect = new Model.DBModel();
	Scanner scanner = new Scanner(System.in);
	List<Model.MemberClass> memberList = new ArrayList<Model.MemberClass>();
	List<String> borrowList = new ArrayList<String>();
	
	//드라이버 로드 시도함
	public void MemberRegister() {
		boolean check=true;
		boolean repeat = true;
		int repeatcheck=0;
		view.ConsoleClear();
		stmt = dbconnect.DBConnect();
		String memberId="";
		String memberPassword="";
		String memberName="";
		String abc = "";
		String query="";
		view.TitleView();
		//회원ID,PW,이름 등을 Console을 통해서 입력받음
		//회원ID의 경우 중복된 ID가 있으면 안되기 때문에 그것을 방지함
		while(repeat) {
			if(repeatcheck==0) {
					System.out.println("회원 ID를 입력해주세요  :  ");
				}
				else {
					System.out.println("중복된 아이디입니다. 다른 회원 ID를 입력해주세요  :  ");
				}
			memberId = scanner.nextLine();
			check = dbconnect.IdCheck(memberId,1);	//중복된 아이디가 있을 경우 true 반환
			if(check) {		//중복된 아이디가 있다면
				repeat = true;
				repeatcheck++;
			}
		else {
			repeat=false;
		}
		}
		System.out.println("회원 PW를 입력해주세요 :  ");
		memberPassword = scanner.nextLine();
		System.out.println("회원 이름을 입력해주세요 :  ");
		memberName = scanner.nextLine();
		//위에서 변수에 저장된 입력된 값들을 DB에 INSERT하는 쿼리문을 작성해서 변수 query에 저장
		System.out.println("엔터를 누르면 다음으로 넘어갑니다.");
		scanner.nextLine();
		dbconnect.DBInsert("library.member(memberid,password,memberName)", "'"+memberId+"','"+memberPassword+"','"+memberName+"'", " ");
		//위에 query에 저장된 쿼리문을 DB에 보내서 실행시키는 함수
		
		//스캐너 닫아줘야한다
		
		// 서버와의 연결도 닫아줌
		//try {connection.close();}
		//catch(SQLException e) {e.printStackTrace();}
	}
	
public String MemberLogin() {
		//try catch로 처리해야 하는 변수들 앞에 미리 선언, 이렇게 안하면 try catch 바깥에서는 변수 사용 불가s
		String competeid="";
		boolean flag = true;
		String competePW = "";
		
		//데이터베이스 연결
		stmt = dbconnect.DBConnect();
		//아래로 내리는 걸로 콘솔 클리어시키고 도서관리프로그램 타이틀을 출력한다
		view.ConsoleClear();
		view.TitleView();
		
		//ID를 입력받는다
		System.out.println("ID를 입력하세요 : ");
		String inputId = scanner.nextLine();
		//입력받은 ID가 DB에 존재하는지 확인하는 쿼리문을 짜서 보낸다
		excute = dbconnect.DBSelect("memberid", "library.member", "WHERE memberid = '"+inputId+"'");
		//쿼리문에 조건을 걸어 보내서 한 행만 출력이 된다
		try {
			if(excute!=null) {
						// 입력된 ID가 DB에 없으면 excute.next()가 false로 나와서 true가 되서 반복문 계속함
				while(!excute.next()){
					excute.close();                    //위에 excute를 닫아준다. 아래 query문을 다시 보내기 위해서
				System.out.println("없는 ID입니다. 다시 입력해 주세요 :  ");
				String testId = scanner.nextLine();                
				
				excute = dbconnect.DBSelect("memberid","library.member", "WHERE memberid = '"+testId+"'");
				
					}
				//excute.last()는 맨마지막 줄로 가서 있으면 true 없으면 false반환, 어차피 한줄 출력이라 있는지 업는지만 확인하는 것과 같다.
			if(excute.last()) {
				competeid = excute.getString("memberid");
				flag = true;
			}
			excute.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
		while(flag) {
			//password 입력받아서 앞에 입력한 ID가 있는 행의 password와 같은지 확인
			System.out.println("PW를 입력하세요 :  ");
			String inputPW=scanner.nextLine(); 
			if(stmt!=null) {
			excute = dbconnect.DBSelect("password", "library.member", "WHERE password ='"+inputPW+"'");
			}
				//만약 비밀번호가 틀렸으면 excute.next()가 false가 나온다. 그래서 맞출때까지 못나옴
			while(!excute.next()) {
				excute.close();
				System.out.println("틀린 비밀번호입니다. 정확한 비밀번호를 입력해주세요 :  ");
				String testPW=scanner.nextLine();
				excute = dbconnect.DBSelect("password", "library.member", "WHERE password='"+testPW+"'");
						}
			//마지막줄로 가서 값이 존재하면 true반환 조건을 건 select 쿼리문이기 때문에 한줄만 출력된다. 즉 조건에 맞는 값이 있는지 없는지 확인하는 역할을 한다
			if(excute.last()) {
			competePW=excute.getString("password");
			flag=false;
						}
			}
		}
		catch(SQLException e) {
			throw new RuntimeException(e);
		}
		
		return competeid;
	}
	//회원 정보를 출력하는 메소드
	
	
	
	
}

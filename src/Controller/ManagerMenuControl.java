package Controller;

import java.io.IOException;
import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;


public class ManagerMenuControl {

	boolean flag1 = true;
	boolean flag = true;
	Model.DBModel dbconnect = new Model.DBModel();
	View.AllView view = new View.AllView();
	Controller.ManagerControl managerControl = new Controller.ManagerControl();
	Controller.MemberControl memberControl = new Controller.MemberControl();
	Controller.BookControl bookControl = new Controller.BookControl();
	Scanner scanner = new Scanner(System.in);
	List<Model.BookClass> bookList = new ArrayList<Model.BookClass>();
	List<Model.MemberClass> memberList = new ArrayList<Model.MemberClass>();
	
	//ManagerMeue를 표시하는 컨트롤러
	public boolean ManagerMenu() {
		//도서관리 프로그램 타이틀과 관리자 메뉴 뷰를 불러온다
		flag=true;
		while(flag) {
		view.ConsoleClear();
		flag1=true;
		while(flag1) {
		view.TitleView();
		view.ManagerMenuView();
		
		//메뉴를 숫자로 선택하게 하고 그 값을 받음(1.회원 리스트 출력, 2.책 리스트 출력, 3.회원 검색하기, 4.회원 삭제하기, 5.책 등록하기, 6.책 삭제하기, 7.책 대여반납기록, 8.초기메뉴로 돌아가기)
		System.out.println("메뉴를 선택하세요 : ");
		int select = Integer.parseInt(scanner.nextLine());
		view.ConsoleClear();
		switch(select){
		case 1:		//회원 리스트 출력하기
			memberList=dbconnect.DBMemberSearch("");
			view.MemberListPrint(memberList);
			break;
		case 2:		//책 리스트 출력하기
			bookList = dbconnect.DBBookSearch("");
			view.BookListPrint(bookList);
			break;
		case 3:		//회원 검색하기
			managerControl.MemberSearch();
			break;
		case 4:
			view.MemberRemove();
			break;
		case 5:
			bookControl.BookInput();
			break;
		case 6:
			view.BookRemove();
			break;
			//초기메뉴로 이동
		case 7:
			view.ReportPrint();
			break;
		case 8:
			flag1=false;
			flag=false;
			break;
		}
		if(select!=8) {
		flag1=view.ConsoleBack();
		}
		}
	}
		return false;
	}
}

package Controller;

import java.io.IOException;
import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.text.*; 
import java.sql.Date;

public class BookControl {
	Connection connection=null;
	Statement stmt=null;
	ResultSet excute = null;
	View.AllView view = new View.AllView();
	Model.DBModel dbconnect = new Model.DBModel();
	List<Model.BookClass> bookList = new ArrayList<Model.BookClass>();
	Scanner scanner = new Scanner(System.in);
	
	
	
	public void BookInput(){
		boolean repeat=true;
		boolean check = true;
		//DB 연결하는 클래스를 불러옴
		Statement stmt = dbconnect.DBConnect();
		//try문 밖에서도 쓸 수 있게 앞에 변수들을 선언함
		int bookId=0;
		int repeatCheck=0;
		String bookName="";
		String bookWriter="";
		int bookAmount=0;
		String abc = "";
		String query="";
		//맨 위에 도서관리프로그램 View를 띄움
		view.TitleView();
		//책 등록에 필요한 정보들을 콘솔로 입력받음
		while(repeat) {
			if(repeatCheck==0) {
		System.out.println("도서 ID를 입력하세요 :  ");				//nextint를 하면 int 부분만 읽는 등 어지러워져서 일부러
			}
			else {
				System.out.println("중복된 ID입니다. 다른 ID를 입력하세요 :  ");				
			}
		bookId = Integer.parseInt(scanner.nextLine());		//nextline으로 받고 parseint로 변환함
		check = dbconnect.IdCheck(String.valueOf(bookId), 2); //도서 ID 중복검사 진행
		if(check) {	//만약 중복된 ID가 존재한다면
			repeat=true;
			repeatCheck++;
		}
		else {
			repeat=false;
		}
		}
		System.out.println("책 제목을 입력하세요 :  ");
		bookName = scanner.nextLine();
		System.out.println("작가 이름을 입력하세요 :  ");
		bookWriter=scanner.nextLine();
		System.out.println("책 수량을 입력하세요 :  ");
		bookAmount=Integer.valueOf(scanner.nextLine());

		
		//위에서 입력받은 값들을 DB에 INSERT하는 쿼리문 작성함
		
		//query ="INSERT INTO library.book(bookid,bookname,writer,count) VALUES("+bookId +",'"+bookName+"','"+bookWriter+"',"+bookAmount+")";
		
		//INSERT 문을 쓸 떄 여러여러 요소를 넣어서 INSERT문을 만들 수 있게 함
		dbconnect.DBInsert("library.book(bookid,bookname,writer,count)","'"+bookId+"','"+bookName+"','"+bookWriter+"','"+bookAmount+"'", " ");
		
		//위에서 작성한 쿼리문을 DB로 보내서 실행함
		/*try{stmt.executeUpdate(query);}
		catch (SQLException e) 
		{ 
			e.printStackTrace();
			
		}
		*/
		
	}
	
	//책 검색 대출
	public boolean BookSearchBorrow(String identy) {
		boolean key=false;
		int repeatcheck = 0;
		String inputNo="";
		String ID = identy;
		int inputId=0;
		//처음에 화면 UI 출력함
		view.ConsoleClear();
		view.TitleView();
		//DBconnect를 통해서 DB연결
		stmt=dbconnect.DBConnect();
		//검색하고 싶은 책 이름을 입력받음
		System.out.print("책 이름을 입력하세요 : ");
		String bookname = scanner.nextLine();
		//DB에서 내가 입력한 책 이름을 포함하는 책들을 리스트로 뽑아서 bookList에 넣어줌
		bookList = dbconnect.DBBookSearch(bookname);
		view.BookListPrint(bookList);	//bookList에 있는 책 정보를 출력함
		while(!key) {
			if(repeatcheck==0) {
		System.out.print("대출할 책의 No를 적어주세요(0은 뒤로가기) : ");
			}
			else {
				System.out.print("범위를 벗어난 값입니다. 제대로 된 No를 입력해주세요.(0은 뒤로가기)");
			}
		inputNo = scanner.nextLine(); 
		//0일 경우 뒤로 간다.
		if(inputNo.equals("0")) {
			return true;
		}
		else {
			if(view.KeyInput(inputNo, 1)) {	//입력된 숫자가 숫자가 아닌 경우 false를 반환
				int testNo=Integer.parseInt(inputNo);
				if(testNo<=0||testNo>bookList.size()) {		//범위 초과할 경우 다시 실행
				key=false;	
				repeatcheck++;
				}
				else {						//범위 초과 아닐 경우 int로 값을 변경하고 빠져나옴
					key=true;
					inputId = bookList.get(Integer.parseInt(inputNo)-1).getBookId();
				}
			}
		}
		}
			
			//inpudID와 같은 DB의 행을 찾아서 그 행의 대출한 도서의 수량을 -1 한다.
			dbconnect.DBUpdate("library.book", "count=count-1", "WHERE bookid='"+inputId+"'");
			//대출했다는 기록을 renthistory DB에 기록한다,날짜와 대출여부 등등 ID=유저아이디, inputID=도서ID
			//현재 날짜를 구하는 함수
			//쿼리에 인서트값 넣어서 DB에서 쿼리문 실행함
			dbconnect.DBInsert("library.renthistory(memberid,bookid,state,date)", "'"+ID+"','"+inputId+"','대출',NOW()", " ");
			//try {connection.close();}
			//catch(SQLException e) {e.printStackTrace();}
			return false;
		
		}
	
	
	//책을 반납하는 메소드, 유저의 ID를 매개변수로 받는다, 그리고 그 유저가 대출한 책의 리스트를 출력한다
	public boolean BookReturn(String identy) {
		dbconnect.DBConnect();	//DB에 연결함
		//처음에 UI화면 출력을 깜빡함
		view.ConsoleClear();
		view.TitleView();
		//USER의 ID를 받음
		String ID = identy;
		bookList.clear();			//북리스트 초기화
		bookList= dbconnect.DBBorrowBookList(ID);		//대출한 책 리스트를 리스트르 반환 받는다
		view.BookListPrint(bookList);		// 리스트에 있는 값을 함수를 이용해서 출력한다
		System.out.println("반납할 책의 NO를 입력해주세요(돌아가려면 0을 입력해주세요) : ");
		String returnId = String.valueOf(bookList.get(Integer.parseInt(scanner.nextLine())-1).getBookId());
		if(returnId.equals("0")) {
			//함수 끝나기 전에 DB 연결 해제
			try {connection.close();}
			catch(SQLException e) {e.printStackTrace();}
			return false;
		}
		else {
			//빌린걸 반납하는 거니까 책의 수량을 +1한다.
			dbconnect.DBUpdate("library.book", "count=count+1", "WHERE bookid='"+returnId+"'");
			//renthistory에서 대출을 반납으로 바꾸고, back에 반납한 날짜를 적는다
			dbconnect.DBUpdate("library.renthistory","state='반납',back=NOW()", "WHERE renthistory.memberid='"+ID+"' AND renthistory.bookid='"+returnId+"'");
			//DB 사용 끝나서 DB연결 해제함
			System.out.println("반납에 성공하셨습니다. ");
			System.out.println("추가로 반납하시겠습니까?? 1 : yes, 2: No");
			int select = Integer.parseInt(scanner.nextLine());
			if(select==1) {
				return true;
			}
			else if (select==2) {
				return false;
			}
			else {
				System.out.println("제대로 된 값을 입력해주세요 : ");
				return true;
			}
		}
	}
}
		
	


package Controller;

import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class MainMenu {

	public static void main(String[] args) {
		boolean flag = true;
		boolean key =false;
		String menuNumber="";
		String identy="";
		int repeat=0;
		Model.DBModel dbconnect = new Model.DBModel();	//DB연결 객체 선언
		View.AllView view = new View.AllView();
		Controller.MemberControl memberControl = new Controller.MemberControl();	//회원 클래스 객체화
		Controller.BookControl bookControl = new Controller.BookControl();		//도서 클래스 객체화
		Controller.MemberMenuControl memberMenu = new Controller.MemberMenuControl();		//로그인 메뉴 클래스 객체화
		Controller.ManagerMenuControl managerMenu = new Controller.ManagerMenuControl();	//관리자 메뉴 클래스 객체화
		Scanner keyRead = new Scanner(System.in);
		boolean flag1 = true;
		while(flag) {
		identy="";
		
		//위에 도서관리프로그램 타이틀 View를 출력함
		view.TitleView();
		view.MainMenuView();
		while(!key) {
			if(repeat==0) {
		System.out.println("메뉴를 선택하세요 : ");
			}
			else {
				System.out.println("잘못된 값입니다. 다시 입력해주세요 :  ");
			}
		menuNumber = keyRead.nextLine();
		
			//입력한 값이 숫자인 경우 true반환
		if(view.KeyInput(menuNumber,1)) {//입력된 숫자가 범위를 벗어나거나, 숫자가 아닐 경우 다시
			int competeKey = Integer.parseInt(menuNumber);
			if(competeKey<=0||competeKey>4) {
				repeat++;
			}
			else {key=true;}
				}
		else {
			key=true;
		}
		}
		//메뉴 선택 switch문 (1. 로그인 / 2. 회원가입 / 3. 관리자메뉴 / 4. 종료)
		switch(menuNumber) {
		case "1":
			identy = memberControl.MemberLogin();
			memberMenu.MemberMain(identy);
			break;
		case "2":
			memberControl.MemberRegister();
			break;
		case "3":
			view.ConsoleClear();
			flag1=view.ManagerPassword();
			while(flag1) {
				flag1 = managerMenu.ManagerMenu();
			}
			view.ConsoleClear();
			break;
		case "4":
			System.exit(0);
			break;
		}
		
		//keyRead.close();
		
		//
		//identy = control1.MemberLogin();
		//control1.MemberRegister();
		}
	}
}




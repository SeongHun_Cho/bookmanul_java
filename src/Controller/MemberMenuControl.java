package Controller;

import java.io.IOException;
import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;



public class MemberMenuControl {
	boolean flag = true;
	boolean flag1;
	boolean key = false;
	int repeat = 0;
	String inputKey;
	Controller.BookControl bookControl = new Controller.BookControl();
	Scanner scanner = new Scanner(System.in);
	
	public void MemberMain(String identy) {
		flag=true;
		repeat=0;
		
		//첫번째 반복문 
		while(flag) {
		//로그인 한 후에 memberID를 기억해놔야 나중에 책 대출 반납 다 할 수 있음
		String ID=identy;
		flag1=true;
		  while(flag1) {
			  key = false;
		View.AllView view = new View.AllView();
		view.ConsoleClear();		//화면을 내리는 걸로 클리어효과
		view.TitleView();			//도서관리프로그램의 Title을 출력한다
		view.LoginMenuView();		//로그인 메뉴를 보여준다
		//메뉴에 맞는 숫자만 받는다. 숫자가 아니면 다시
		while(!key) {
			if(repeat==0) {
		System.out.println("번호를 입력해주세요 : ");
			}
			else {
				System.out.println("잘못된 값을 입력하셨습니다. 다시 입력해주세요: ");
			}
		inputKey = scanner.nextLine();
		
			if(view.KeyInput(inputKey,1)) {//입력된 값이 숫자가 맞을 경우에 범위 등을 비교
				int competeKey = Integer.parseInt(inputKey);
				if(competeKey==0||competeKey>5) {//범위를 벗어나는지 비교
					repeat++;					// 출력될 문구를 다르게 하기 위해서 repeat 변수를 씀
					}
				else {
					key=true;					// 예외처리 해당안되면 빠져나간다.
				}
				}
			else {
				key=false;						// 예외요소가 있으면 반복
			}
		}
		// 메뉴 번호를 입력받는 switch 문 (1. 책 검색/대출 / 2. 책 반납 / 3. 회원 정보 / 4. 초기 메뉴 / 5. 종료)
		switch(inputKey){
			case "1":
				flag1 = bookControl.BookSearchBorrow(ID);
				break;
			case "2":
				flag1 = bookControl.BookReturn(ID);
				break;
			case "3":
				flag1=view.MemberInfoControl(ID);
				break;
			case "4":
				flag1=false;
				flag=false;
				view.ConsoleClear();
				break;
			case "5":
			    System.exit(0);
				break;
				
		}
		
		  }
		}
	}
}

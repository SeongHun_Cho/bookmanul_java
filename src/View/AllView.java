package View;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AllView {

	List<Model.BookClass> bookList = new ArrayList<Model.BookClass>();
	List<Model.MemberClass> memberList = new ArrayList<Model.MemberClass>();
	List<String> borrowList = new ArrayList<String>();
	Model.DBModel dbconnect = new Model.DBModel();
	Scanner scanner = new Scanner(System.in);
	int amount;
	List<String> reportList = new ArrayList<String>();
	
	public void TitleView() {
		System.out.println("◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈");
		System.out.println("도         서          관          리          프           로          그          램");
		System.out.println("◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈◈\n\n");
	}
	
	
	public void MainMenuView() {
		System.out.println("                                 ▶ 1. 로그인");
		System.out.println("                                 ▶ 2. 회원가입");
		System.out.println("                                 ▶ 3. 관리자메뉴");
		System.out.println("                                 ▶ 4. 종료");
	}
	
	
	public void LoginMenuView() {
		System.out.println("                                 ▶ 1. 책 검색/대출");
		System.out.println("                                 ▶ 2. 책 반납");
		System.out.println("                                 ▶ 3. 회원 정보");
		System.out.println("                                 ▶ 4. 초기 메뉴");
		System.out.println("                                 ▶ 5. 종료");
	}
	
	
	public void ManagerMenuView() {
		System.out.println("                                 ▶ 1. 회원 리스트");
		System.out.println("                                 ▶ 2. 책 리스트");
		System.out.println("                                 ▶ 3. 회원 검색");
		System.out.println("                                 ▶ 4. 회원 삭제");
		System.out.println("                                 ▶ 5. 신규책등록");
		System.out.println("                                 ▶ 6. 책삭제");
		System.out.println("                                 ▶ 7. 대여/반납기록");
		System.out.println("                                 ▶ 8. 초기 메뉴");
	}
	
	
	public void ConsoleClear() {
		for(int i = 0;i<80;i++) {
		System.out.println("");
		}
	}
	
	//뒤로가는 메소드, false를 받으면 뒤로간다(while반복문에서 빠져나오게 하는 방법으로)
	public boolean ConsoleBack() {
		System.out.println("\n                                 ▶ 1. 뒤로       가기");
		System.out.println("                                 ▶ 2. 프로그램 종료");
		System.out.print("값을 입력하세요. : ");
		String select = scanner.nextLine();
		if(select.equals("1")) {
			return false;
		}
		else {
			System.exit(0);
			return true;
		}
		
	}
	//받은 리스트값을 바탕으로 책 리스트를 출력하는 메소드
	public void BookListPrint(List<Model.BookClass> List) {
		bookList=List;
		int number=0;
		for(int roop = 0;roop<bookList.size();roop++) {
			number++;
			System.out.println("------------------------------------------\n");
			System.out.println("No : '"+number);
			System.out.println("도서ID : ID"+bookList.get(roop).getBookId());
			System.out.println("도서제목 : "+bookList.get(roop).getBookName());
			System.out.println("작가  :  "+bookList.get(roop).getWriter());
			System.out.println("수량  :  "+bookList.get(roop).getCount());
			System.out.println("------------------------------------------\n");
		}
	}
	
	public void MemberListPrint(List<Model.MemberClass> List) {
		memberList=List;
		int number = 0;
			for(int roop = 0;roop<memberList.size();roop++) {
				number++;
				amount = dbconnect.DBborrowBookCount(memberList.get(roop).getMemberid());
				System.out.println("\n\n--------------------------------------");
				System.out.println("No : '"+number);
				System.out.println("회원 ID : '"+memberList.get(roop).getMemberid());
				System.out.println("회원 이름 : '"+memberList.get(roop).getMembername());
				System.out.println("대출 권수 : "+amount);
				System.out.print("");
			}
			
	}
	
	public  boolean MemberInfoControl(String identy) {
		String ID = identy;		//아이디를 매개변수로 받음
		memberList=dbconnect.DBMemberInformation(ID);
		borrowList = dbconnect.DBMemberBorrowBook(ID);
		
		System.out.println("          회원 ID :  " + memberList.get(0).getMemberid());
		System.out.println("          회원 이름 :  " + memberList.get(0).getMembername());
		System.out.println("          대출 중인 책 ");
		for(int roop=0;roop<borrowList.size();roop++) {
			System.out.print(" '"+borrowList.get(roop)+"' |");
		}
		boolean flag = ConsoleBack();
		return flag;
		
	}
	
	public void BorrowBookListPrint(List<String> list) {
		
	}
	
	public boolean ManagerPassword() {
		System.out.println("관리자 비밀번호를 입력하세요 :");
		String inputId = scanner.nextLine();
		boolean flag = dbconnect.DBManagerLogin(inputId);
		return flag;
	}
	
	public String MemberSearch() {
		System.out.println("\n찾고자 하는 회원 이름을 입력하세요 : ");
		String searchName = scanner.nextLine();
		return searchName;
	}
	
	//관리자메뉴에서 회언삭제하는 메소드
	public void MemberRemove() {
		System.out.println("삭제하고 싶은 회원의 이름을 입력하세요 : ");
		String removeMember = scanner.nextLine();
		memberList = dbconnect.DBMemberSearch(removeMember);			//삭제하고자하는 사람 이름을 검색한다
		MemberListPrint(memberList);			//리스트로 뺀 값을 콘솔사에 출력
		System.out.println("삭제하고자 하는 회원의 번호를 입력하세요 : ");
		int no = Integer.parseInt(scanner.nextLine())-1;
		String queryNumber = memberList.get(no).getMemberid();
		dbconnect.DBDeleteQuery(queryNumber,1);			//DELETE 쿼리문 실행하는 함수(1이면 member삭제, 2이면 book삭제)
		memberList.remove(no);					//리스트의 값을 REMOVE로 지움
	}
	
	public void BookRemove() {
		System.out.println("삭제하고 싶은 책의 이름을 검색하세요 : ");
		String removeBook = scanner.nextLine();
		bookList = dbconnect.DBBookSearch(removeBook);
		BookListPrint(bookList);
		System.out.println("삭제하고자 하는 책의 번호를 입력하세요 : ");
		int no = Integer.parseInt(scanner.nextLine())-1;
		int queryNumber = bookList.get(no).getBookId();
		
		dbconnect.DBDeleteQuery(String.valueOf(queryNumber), 2);
		bookList.remove(no);
	}
	
	public void ReportPrint() {
		reportList=dbconnect.DBReport();
		for(int repeat=0;repeat<reportList.size();repeat++) {
			System.out.println(reportList.get(repeat));
		}
	}
	//1일 떄 숫자만 출력,2일 떄 영어만 출력,3일 때는 한글만 출력
	public boolean KeyInput(String ik,int md) {
		String inputKey= ik;
		int mode = md;
		char chartest;
		//1일 때는 숫자만 출력하게
		if(md==1) {
		for(int roop = 0; roop<inputKey.length();roop++) {
			chartest = inputKey.charAt(roop);
		if(chartest>='0'&&chartest<='9') {
			//문제 없음
			//스프링 출력
			System.out.println("숫자임");
		}
		else {
			return false;
		}
		}
		return true;
		}
		//2일 때는 영어랑 숫자만 출력
		else if(md==2) {
			for(int roop = 0; roop<inputKey.length();roop++) {
				chartest = inputKey.charAt(roop);			
			if((chartest>='0'&&chartest<='9')||(chartest>='a'&&chartest<='z')||(chartest>='A'&&chartest<='Z')) {
				//문제 없음
			}
			else {
				return false;
			}
			}
			return true;
			}
		//3일 떄는 한글만 출력
		else if(md==3) {
			for(int roop = 0; roop<inputKey.length();roop++) {
				chartest=inputKey.charAt(roop);
				if((chartest>='가'&&chartest<='힣')) {
						//문제없음
				}
				else {
					return false;
					}
		}
			return true;
		}
		else {return false;}
	}
}

